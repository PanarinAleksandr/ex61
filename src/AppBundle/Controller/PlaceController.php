<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class PlaceController extends Controller
{
    /**
     * @Route("/")
     */
    public function indexAction()
    {

        $places = $this->getDoctrine()
                      ->getRepository('AppBundle:Place')
                      ->findAll();
        $arr = [];
         foreach ($places as $place){
             $a = date("d.m.Y",strtotime("Jan 01 2017"));
             $dishes = $this->getDoctrine()->getRepository('AppBundle:Dish')
                 ->getMostPopular($place,2, $a );
             $arr[] = [
                 'place'=>$place,
                 'dishes'=>$dishes
             ];
         }
        return $this->render('@App/Place/index.html.twig', array(
           'datas' => $arr
        ));
    }

    /**
     * @Route("/detail/{id}",name="detail" ,requirements={"id" : "\d+"})
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
     public function detailAction(int $id)
     {
         $plece =$this->getDoctrine()
                  ->getRepository('AppBundle:Place')
                  ->find($id);

         return $this->render('@App/Place/detail.html.twig', array(
             'plece'=> $plece
         ));
     }

}
