<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Purchase;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPurchasesData extends Fixture implements DependentFixtureInterface
{


    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $arrDihes = LoadDishData::getDishes();

        for ($i = 0; $i < 300; $i++) {
            $randDish = rand(0, 29);
            $purchases = new Purchase();
            $timestamp = rand( strtotime("Jan 01 2016"), time() );
            $random_Date = date("d.m.Y", $timestamp );
            $purchases->setDate(new \DateTime($random_Date))
                      ->setDish( $this->getReference($arrDihes[$randDish]));
            $manager->persist($purchases);
        }
        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
       return [
         LoadDishData::class
       ];
    }
}