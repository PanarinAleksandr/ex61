<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Place;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class LoadPlaceData extends Fixture
{
    const PLECE_ONE = 'Faiza';
    const PLECE_TWO = 'Arzu';

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $res1 = new Place();
        $res1->setName(self::PLECE_ONE)
            ->setDescription('Какое то описане Фаиза')
            ->setImage('Faiza.jpg');
        $manager->persist($res1);

        $res2 =  new Place();
        $res2->setName(self::PLECE_TWO)
            ->setDescription('Какое то описане Арзу')
            ->setImage('arzu.jpg');
        $manager->persist($res2);

        $manager->flush();

        $this->addReference(self::PLECE_ONE,$res1);
        $this->addReference(self::PLECE_TWO,$res2);
    }


}