<?php

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\Dish;
use AppBundle\Entity\Restaurant;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadDishData extends Fixture implements DependentFixtureInterface
{
    private static $dishes =[];

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $dish = [
            'Ашлям — фу' => 'ashlymfy.jpg', 'Яичница глазунья' => 'Fried_egg.jpg', 'Лагман' => 'Lagman.jpg',
            'Ган-фан' => 'Gan-fan.jpg', 'Бифштекс с яйцом(с гарниром)' => 'Steak_with_egg.jpg',

            'Манты'=> 'ashlymfy.jpg', 'Гулящ' => 'Fried_egg.jpg', 'Самсы' => 'Lagman.jpg',
            'Шашлык' => 'Gan-fan.jpg', 'Гюро — Лагман' => 'Steak_with_egg.jpg', 'Мампар' => 'ashlymfy.jpg',
            'Борш' => 'ashlymfy.jpg', 'Чучвара' => 'ashlymfy.jpg',
            'Котлета в кляре(с гарниром)'=> 'ashlymfy.jpg', 'Плов по — узбекски' => 'ashlymfy.jpg'
        ];
        $rest1 = $this->getReference(LoadPlaceData::PLECE_ONE);
        $rest2 = $this->getReference(LoadPlaceData::PLECE_TWO);
        $place_name = [$rest1, $rest2];
        $count = 0;
        for($i = 0; $i< count($place_name); $i++) {
            foreach ($dish as $name => $image) {
                $dishes = new Dish();
                $dishes->setName($name)
                    ->setPrice(rand(50, 200))
                    ->setImage($image)
                    ->setPlace($place_name[$i]);
                $manager->persist($dishes);
                $count++;
                $dishId ="dish {$count}";
                $this->addReference($dishId, $dishes);
                self::$dishes[] = $dishId;
            }
        }
        $manager->flush();

    }
    function getDependencies()
    {
        return [
            LoadPlaceData::class
        ];
    }
    public static function getDishes(){
        return self::$dishes;
    }
}